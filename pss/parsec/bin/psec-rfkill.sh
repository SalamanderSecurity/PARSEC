#!/bin/bash

if [[ "$1" == "block" ]]; then
	rm /tmp/psec-unblock
	rfkill block all
elif [[ "$1" == "unblock" ]]; then
	touch /tmp/psec-unblock
	chmod 000 /tmp/psec-unblock
	if [[ "$#" != 1 ]]; then
		rfkill unblock "$2"
	fi
else
	echo "Invalid command. Usage: $0 [block | unblock]"
fi
