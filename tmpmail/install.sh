#!/bin/bash

if [[ "$UID" != 0 ]]; then
	echo "$0 must be run as root."
	exit
fi

cd script/
while read line; do
	install "$line" /usr/bin/
	chmod u=rwx,g=rx,o=rx "/usr/bin/$line"
done <<< "pc-elinks
pc-links
pc-lynx
pc-w3m
tmpmail
tmpmail.nx.sh"
