#!/bin/bash

if [[ "$UID" != 0 ]]; then
	echo "Must be run as root."
	sudo "$0" "$@"
	exit
fi

OWD="$PWD"

echo "This script is created by Babba27, also known as DriftNotSkid."
echo "This script installs Babba27's custom configuration on your system. Setting up the PARSEC suite of scripts is the primary purpose of this script, but it also allows you to install additional hardening and tools from a single place."

if [[ "$(pacman -Q runit)" == "" ]]; then
	echo "Warning: This script has only been tested using the Runit init system."
fi
echo -n "Continue? y/n: "
read continue
if [[ "$continue" != y ]]; then
	echo "User quit."
	exit
fi

echo -n "Install all PARSEC scripts? y/n: "
read parsec
echo -n "Install required dependencies? y/n: "
read instdeps
echo -n "Install optional additional tools? y/n: "
read addons
echo -n "Install custom bashrc changes? y/n: "
read bashrc
echo -n "Replace Linux with hardened kernel? y/n: "
read hardenedkernel
#if [[ "$hardenedkernel" == y ]]; then
#	echo "Installing a self-compiled kernel is not yet supported. This will be added in a future update."
#	echo -n "Use self-compiled hardened kernel? (This will take a long time to compile) y/n: "
#	read selfcomp
#fi
echo -n "Install hardened malloc to system-wide LD_PRELOAD? y/n: "
read hmalloc
if [[ "$(pacman -Q runit)" == "" ]]; then
	echo "Warning: hardened boot parameters have only been tested on the Runit init system."
	echo "Other inits may break."
fi
echo "Install hardened boot parameters? This may break certain software, but is fine on headless base systems."
echo -ne "\tSelect 'n' if your system uses systemd. y/n: "
read bootparam
echo -n "Install sysctl hardening? (may break certain software, but fine on headless base systems) y/n: "
read sctl
echo -n "Install /proc as hidepid 2? (Useful for preventing process info leaks) y/n: "
read hidepid
echo -n "Install AppArmor? y/n: "
read apparmor
echo -n "Install proxychains? y/n: "
read proxychains
echo -n "Install iptables and comfigure default rules? y/n: "
read iptables
if [[ "$(pacman -Q runit)" != "" ]]; then
	echo -n "Install custom runit services? y/n: "
	read runit
fi
echo -n "Install tmpmail, a CLI-based method of generating burner emails over Tor? y/n: "
read tmpmail

if [[ "$parsec" == y ]]; then
	mkdir -p /parsec/bin/
	cp parsec/bin/* /parsec/bin/
	chmod u=rx,g=rx,o=rx /parsec/bin/*
	chmod u+s,g+s,o+s /parsec/bin/psec-rfkill
	chmod u=r,g=r,o=r /parsec/bin/pkgbuild.patch
fi
if [[ "$instdeps" == y ]]; then
	echo "Are you running a headless base system? Headless meaning no GUI (X.org/Wayland) is installed."
	echo -n "y/n: "
	read headless
	pacman -S --noconfirm trizen
	if [[ "$headless" ]]; then
		userconfirmed=0
		while [[ "$userconfirmed" != 1 ]]; do
			echo "To install aur/fbvnc, Trizen should be run as a normal user, not root. This is for security reasons."
			echo "To run Trizen as a normal user, enter the name of the user. This user should have access to 'sudo'."
			echo "To run Trizen as root, leave the username empty. This is not recommended for security reasons."
			echo
			echo -n "Username: "
			read username
			if [[ "$username" == "" ]]; then
				echo -n "Are you sure you want to run Trizen as root? y/n: "
				read runasroot
				if [[ "$runasroot" == y ]]; then
					userconfirmed=1
				else
					continue
				fi
				trizen -S --noconfirm aur/fbvnc-git
			else
				userconfirmed=1
				sudo -u "$username" trizen -S --noconfirm aur/fbvnc-git
			fi
			pacman -S --noconfirm qemu-headless
		done	
	else
		pacman -S --noconfirm qemu
	fi
	pacman -S --noconfirm bubblewrap-suid screen tor
    echo "Installing base-devel..."
    echo "If your system is not up-to-date, open a separate terminal and run 'pacman -Syu' right now."
    echo "Installing base-devel without first updating *will* break your system, requiring an OS reinstall."
    echo "Press ENTER to continue, or Ctrl+C to abort. If you press Ctrl+C, you can run this again after updating your system."
    read
    pacman -S base base-devel
#	chmod u+s,g+s,o+s /usr/bin/bwrap
	if [[ "$(pacman -Q runit)" != "" ]]; then
		pacman -S --noconfirm tor-runit
	fi
fi
if [[ "$addons" == y ]]; then
	pacman -S --noconfirm newsboat trizen mplayer mpv weechat elinks links lynx w3m
fi
if [[ "$bashrc" == y ]]; then
	cat bashrc >> /etc/bash/bashrc
fi
if [[ "$hardenedkernel" == y ]]; then
	pacman -S --noconfirm linux-hardened linux-hardened-headers
	update-grub # I use update-grub between every change in case the install fails, to avoid the system entering an unbootable state
	pacman -R --noconfirm linux
	update-grub
	pacman -S --noconfirm pacman-contrib
	if [[ "$selfcomp" == y ]]; then
# validpgpkeys=(
#   'ABAF11C65A2970B130ABE3C479BE3E4300411886'  # Linus Torvalds
#   '647F28654894E3BD457199BE38DBBDC86092693E'  # Greg Kroah-Hartman
#   'E240B57E2C4630BA768E2F26FC1B547C8D8172C8'  # Levente Polyak
# )
#		/usr/bin/gpg --recv-keys E240B57E2C4630BA768E2F26FC1B547C8D8172C8
#		/usr/bin/gpg --recv-keys 647F28654894E3BD457199BE38DBBDC86092693E
#		/usr/bin/gpg --recv-keys ABAF11C65A2970B130ABE3C479BE3E4300411886
		
		mkdir -p /parsec/bin/
		cp parsec/bin/psec-update-kernel /parsec/bin/
		chmod u=rx,g=rx,o=rx /parsec/bin/psec-update-kernel
		/parsec/bin/psec-update-kernel
		update-grub  # I use update-grub between every change in case the install fails, to avoid the system entering an unbootable state
		pacman -R --noconfirm linux-hardened
		pacman -R --noconfirm linux
		update-grub
	fi
fi
if [[ "$hmalloc" == y ]]; then
	unzip hardened_malloc-8.zip
	cd hardened_malloc-8
	make
	install libhardened_malloc.so /usr/local/lib/libhardened_malloc.so
	chmod u=rx,g=rx,o=rx /usr/local/lib/libhardened_malloc.so
	cp ../ld.so.preload /etc/ld.so.preload
	chmod u=r,g=r,o=r /etc/ld.so.preload
	cd ..
	rm -r hardened_malloc-8/
fi
if [[ "$bootparam" == y ]]; then
	echo 'GRUB_CMDLINE_LINUX_DEFAULT+=" l1tf=full,force mds=full,nosmt mitigations=auto,nosmt nosmt=force amd_iommu=on kernel.yama.ptrace_scope=2 lockdown=confidentiality slab_nomerge slub_debug=FZ init_on_alloc=1 init_on_free=1 page_alloc.shuffle=1 pti=on vsyscall=none debugfs=off oops=panic module.sig_enforce=1 lockdown=confidentiality quiet loglevel=0 spectre_v2=on spec_store_bypass_disable=on tsx=off tsx_async_abort=full,nosmt mds=full,nosmt l1tf=full,force nosmt=force kvm.nx_huge_pages=force nousb"' >> /etc/default/grub
	update-grub
fi
if [[ "$sctl" == y ]]; then
	cp sysctl.d/* /etc/sysctl.d/
	sysctl --system	
fi
if [[ "$hidepid" == y ]]; then
	echo "proc /proc proc nosuid,nodev,noexec,hidepid=2,gid=proc 0 0" >> /etc/fstab
fi
if [[ "$proxychains" == y ]]; then
	pacman -S --noconfirm proxychains
	cp proxychains.conf /etc/
fi
if [[ "$iptables" == y ]]; then
	pacman -S --noconfirm iptables nftables
	cp iptables.rules /etc/iptables/
	cp ip6tables.rules /etc/iptables/
fi
if [[ "$tmpmail" == y ]]; then
	cd tmpmail
	bash ./install.sh	
	cd "$OWD"
fi
if [[ "$apparmor" == y ]]; then
	pacman -S --noconfirm apparmor
	echo 'GRUB_CMDLINE_LINUX_DEFAULT+=" apparmor=1 security=apparmor"' >> /etc/default/grub
	update-grub
	if [[ "$(pacman -Q runit)" != "" ]]; then
		pacman -S --noconfirm apparmor-runit
	fi
fi
if [[ "$runit" == y ]]; then
	for d in runit/*/; do
		cp "$d"/run "/etc/runit/sv/$(basename "$d")/"
	done	
	echo "Installed services: $(ls runit/ | tr '\n' ' ')"
	echo "To start them on boot, you must manually setup symbolic links in '/run/runit/service/':"
	echo "Use the following command to enable a service on boot:"
	echo '	`ln -s /etc/runit/sv/<service> /run/runit/service/`'
fi

cat << EOF

Your system has been setup. What to do now?

- Setup secure boot using your own keys
	- See the Arch Wiki's documentation on secure boot
- Setup VMs to isolate your activities
	- This is explained in the README.md
- Reconfigure filesystem permissions
	- Linux, by default, allows unprivileged users to read files they don't own. It may be a good idea to change this
- Install microcode updates for your system

- If you notice any bugs or missing dependencies, contact Babba27 on Matrix
	- @babba27:fantasycookie17.onederfultech.com
EOF
