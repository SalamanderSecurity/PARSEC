# PARSEC Script Suite
## A set of scripts for minimal hardened systems

#### A collection of system hardening and generally-useful scripts for Linux systems. Targets Artix, works on any distro. Some inits require disabling the included boot parameter hardening, otherwise they won't boot (systemd)

### Special thanks to  Whonix developer Madaidan, who put together a comprehensive [Linux hardening guide](https://madaidans-insecurities.github.io/guides/linux-hardening.html) that serves as the primary basis for this, as well as the [Arch Wiki's security guide](https://wiki.archlinux.org/index.php/Security).

NOTE: These scripts are only tested using the `runit` init system. Things that may break with other inits:

- Custom startup scripts (used to mitigate info leaks, apply iptables rules, and disable wireless interfaces on startup).
- Some included boot parameter hardening options may prevent the system from booting. You may be able to debug the offending options manually.
	- Additionally, some hardening conflicts with `systemd`. `systemd` is not recommended anyway. Note that the included hardening may break systems that use `systemd`.
	- If you encounter these issues, remove the boot parameter hardening options from your GRUB config.
- This script assumes you use the GRUB bootloader. While other bootloaders will also work, the included boot hardening options will only apply successfully on systems that use GRUB.

### Installing:

All dependencies should be installed automatically. If a dependency is missed, contact me over Matrix: `@babba27:fantasycookie17.onederfultech.com`.

### Running VMs:

For running VMs, the associated files are stored by default inside `/parsec/sdb1/user/$USER/vm/`. On my own configuration, `/parsec/sdb1` is a separate drive mounted at boot. To use VMs from a different path, either create a bind mount, or modify the scripts `psec-vm` and `psec-vm-iso` to reflect the new directory.

#### Creating a VM:

- Create the VM directory: `mkdir -p /parsec/sdb1/user/$USER/vm/$VM_NAME`.
- Create a drive image: `qemu-img create /parsec/sdb1/user/$USER/vm/$VM_NAME/hdd.img 20G` (replace 20G with the desired size of the VM's primary drive).
- Copy an installer ISO to the VM directory: `cp $PATH_TO_MY_ISO /parsec/sdb1/user/$USER/vm/$VM_NAME/cd.iso`.
- Run the VM from the installer ISO: `psec-vm-iso $VM_NAME`.
- After installing, run the VM normally: `psec-vm $VM_NAME`.
	- If using a headless system, QEMU will open a VNC server on loopback. To interact with the VM, run the command `fbvnc`. This will open a VNC session in the framebuffer.
		- To use a mouse within a VM, you must setup USB forwarding. The default config forwards my mouse and USB bluetooth adapter to the VM. If you use a different mouse than I do, you'll need to reconfigure it to use your own hardware.
		- Alternatively, if you're not using a headless system, mouse input should be forwarded automatically.

### Other scripts:

Most of the included scripts are self-explanatory. Some noteworthy scripts include:

- `psec-gpg`: A wrapper to run GPG inside a Bubblewrap sandbox.
- `fbrng`: An RNG that draws generated randomness to the framebuffer.
- `psec-rng`: An RNG that increases entropy by combining 2 low-entropy sources: `cat $SOURCE_1 | psec-rng $SOURCE_2`. This can also be used to mitigate suspected backdoors in hardware RNGs: `hasciicam | sudo psec-rng /dev/urandom` (You must install `hasciicam` manually, since the setup script does not install it).
- `af-upload`: A script to upload files to anonfiles.com. A valid API key is included, so you don't need to create an account.
- `ttm-upload`: A replacement for `af-upload` that doesn't require people to go through a landing page before downloading.

### Additional notes:

- This script uses Daniel Micay's hardened malloc, version 6. The previous version of this script bundled it as a precompiled binary, while this version builds it from source.
- `psec-rng` and `fbrng` are included in compiled form. I developed them myself, and will be releasing the source separately. They are licensed under the BSD 3-clause license.

Also,
- If you get this script to work on another init system, let me know.
- Or, if you use it (successfully or unsuccessfully) on an init system it hasn't yet been tested on, let me know.

I would love to add additional compatibility and learn more about what systems this script is/isn't compatible with.

- If you get this script's boot parameter hardening to work on a bootloader other than GRUB, let me know.
- If you find any compatibility issues, let me know.
- If you fix any compatibility issues, let me know.

### Donating:

I've been asked to provide a donation address. I have no idea why in the hell you'd want to donate to me, but if for some reason you want to, you can send donations to the following Monero address:
```
442zYXVLBsB9VvswpTr3z3V5y2Sn55a4V3NGfRXHt2JF59Zb3VS4CZ4ReymnZNxzi56sPpWjsfbtzesR6JW49eBFK44SJLL
```
